import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatRadioModule, MatInputModule, MatSelectModule,
  MatButtonModule, MatIconModule, MatProgressSpinnerModule, MatExpansionModule,
  MatFormFieldModule, MatCardModule, MatDatepickerModule, MatSlideToggleModule
} from '@angular/material';


@NgModule({
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatButtonModule,
    MatRadioModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    MatSlideToggleModule,
    MatFormFieldModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatExpansionModule
  ]
})
export class SharedMaterialFormModule { }
