import { NgModule } from '@angular/core';
import { LayoutModule } from '@angular/cdk/layout';
import {
  MatToolbarModule,
  MatButtonModule,
  MatMenuModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatGridListModule,
  MatProgressSpinnerModule,
  MatExpansionModule,
  MatCardModule,
  MatSnackBarModule,
  MatPaginatorModule,
  MatProgressBarModule
} from '@angular/material';

@NgModule({
  // imports: [
  //   LayoutModule,
  //   MatToolbarModule,
  //   MatButtonModule,
  //   MatMenuModule,
  //   MatSidenavModule,
  //   MatIconModule,
  //   MatListModule,
  //   MatGridListModule,
  //   MatProgressBarModule,
  //   MatProgressSpinnerModule,
  //   MatExpansionModule,
  //   MatCardModule,
  //   MatPaginatorModule
  // ],
  exports: [
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatMenuModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatExpansionModule,
    MatCardModule,
    MatSnackBarModule,
    MatPaginatorModule
  ]
})
export class SharedMaterialCoreModule { }
