import { NgModule } from '@angular/core';
import { LayoutModule } from '@angular/cdk/layout';
import {
  MatButtonModule,
  MatMenuModule,
  MatIconModule,
  MatListModule,
  MatGridListModule,
  MatProgressSpinnerModule,
  MatCardModule
} from '@angular/material';

@NgModule({
  exports: [
    LayoutModule,
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatProgressSpinnerModule,
    MatCardModule
  ]
})
export class SharedMaterialMinModule { }
