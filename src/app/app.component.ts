import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Safety Database Monitoring';
  selectedLang: string;

  constructor(
    public translate: TranslateService
  ) {
    // add available languages
    translate.addLangs(['en', 'it']);
    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang('en');
    // the lang to use, if the lang isn't available, it will use the current loader to get them
    const browserLang = translate.getBrowserLang();
    // TODO: temporarily set en as default language
    // const usedLang = browserLang.match(/en|it/) ? browserLang : 'en';
    const usedLang = 'en';
    translate.use(usedLang);
    this.selectedLang = usedLang;
  }

}
